//playing with inheretance
import { Human } from "./Human.js";

export class Lawyer extends Human{
    constructor(human,experience){
        super(human.firstname,human.lastname);
        this.experience=experience;
    }

}

Object.assign(Lawyer.prototype,{
    sayHello(){
        console.log('hello, i\'m a lawyer')
    }
})
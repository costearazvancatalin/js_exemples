//playing around with classes and ptototipes

export class Human {
    constructor(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
   
}
Human.prototype.init = function(firstname, lastname){
    this.firstname = firstname;
    this.lastname = lastname;
}
//getters setters
Object.assign(Human.prototype,{
    setLastname(lastname){
        this.lastname= lastname;
    },
    setFirstname(firstname){
        this.firstname = firstname;
    }
})
//functionality
Object.assign(Human.prototype,{
    sayHello(){
        console.log('hello, i\'m human')
    }
})